# OpenLayers Workshop

OpenLayers is a JavaScript module that allows us to render geospatial information on a map in the browser.

It will be the tool end-users work with to interact with our geospatial data.

It works by rendering a map object, and then layers are added to the map object that contain geospatial information. The map and layers create an overlay with the geospatial information the end users need in a familiar map context.

To start getting our feet wet with OpenLayers we will be working through:

- Introduction
- Basics
- Vector Data

The chapters from the [OpenLayers hosted workshop](https://openlayers.org/workshop/en/)

# Setup

In order to work through this workbook the base instructions have us downloading and unzipping a project release. For your convenience this zip file is at the root of this git repo (openlayers-workshop-en.zip).

You will need to unzip this file which will create a new directory (openlayers-workshop-en/) for you.

```bash
unzip openlayers-workshop-en.zip
```

After unzipping the file you will need to change into the directory that was created for you.

```bash
cd openlayers-workshop-en
```

Looking into this folder you will see it's a JavaScript NPM project!

```bash
ls
```

You can see a package.json file that defines the libraries and custom NPM scripts.

Let's take a look at the package.json

```json
{
  "name": "ol-workshop",
  "description": "OpenLayers workshop",
  "version": "0.0.0",
  "private": true,
  "main": "main.js",
  "dependencies": {
    "colormap": "2.3.1",
    "ol": "6.0.0",
    "ol-hashed": "2.0.0",
    "ol-mapbox-style": "4.3.1"
  },
  "devDependencies": {
    "copy-webpack-plugin": "5.0.2",
    "css-loader": "2.1.1",
    "eslint": "5.16.0",
    "eslint-config-openlayers": "11.0.0",
    "extract-text-webpack-plugin": "3.0.2",
    "html-webpack-plugin": "3.2.0",
    "kompas": "0.0.1",
    "style-loader": "0.23.1",
    "webpack": "4.29.6",
    "webpack-cli": "^3.3.0",
    "webpack-dev-server": "3.2.1"
  },
  "eslintConfig": {
    "extends": "openlayers"
  },
  "scripts": {
    "lint": "eslint .",
    "test": "npm run lint",
    "start": "webpack-dev-server --mode=development",
    "build": "webpack --mode=production"
  }
}
```

There is some project metadata at the top (name, description, version, etc)

There are a bunch of dependencies sorted into dependencies and devDependencies (colormap, ol, eslint, etc)

There are four NPM scripts (lint, test, start, and build)

We need to install the modules listed as the dependencies of the package.json file. We can do this with the NPM install command:

```bash
npm install
```

After you run this command you will see that NPM installed all of the node_modules necessary for this project. They are in a new node_modules directory at the root of this NPM project. List the contents of this directory to see the new folder.

Finally we need to execute one of the NPM scripts to start this project.

```bash
npm run start
```

This should kick off the project and it will be live at [localhost:3000](http://localhost:3000)

To see that everything is working you will see an alert box when loading the page above.

The workshop will have us overwriting the provided index.html file and the main.js file to see how OpenLayers works.

# Tasks

Work through the chapters:

- Basics
- Vector Data

Take your time reading the sections and reading over the provided code. We will be continuing to explore OpenLayers tomorrow, in our first project week, and throughout this class.

If you finish early look over some of the additional chapters of the book, nothing from those chapters will be necessary for you to work with OpenLayers in this class, but may be useful information if you will be working with OpenLayers in your job.

Also check out the Bonus links at the bottom for a sneak peek at what we will be exploring tomorrow as a class with OpenLayers.

# Links

- [OpenLayers API Documentation](https://openlayers.org/en/latest/apidoc/)
- [OpenLayers Workshop](https://openlayers.org/workshop/en/)

## Bonus Links

- [OpenLayers WMS Tiles Example](https://openlayers.org/en/latest/examples/wms-tiled.html)
- [OpenLayers WFS Get Feature Example](https://openlayers.org/en/latest/examples/vector-wfs-getfeature.html)
- [Additional OpenLayers Examples](https://openlayers.org/en/latest/examples/)
